# Notes:
* Might need to change [moisture sensor](https://youtu.be/udmJyncDvw0)
* [WebSocketsClient](https://www.mischianti.org/2020/12/07/websocket-on-arduino-esp8266-and-esp32-client-1/) on arduino
* [Voltage regulator](https://diyi0t.com/best-battery-for-esp8266/) on NodeMCU 1.0 (ESP8266 12E Arduino-compatible breakout)
