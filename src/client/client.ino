#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
#include <WifiClient.h>
#include <WebSocketsClient.h>


const char* ssid = "data_iot";
const char* password = "elev1234";
const bool dhcp = true;

const char* server = "10.1.0.165";
const int port = 8081;
const int update_freq = 1000;

// ignored with dhcp = true
IPAddress static_ip(192, 168, 42, 19);
IPAddress gateway(192, 168, 43, 1);
IPAddress subnet(255, 255, 255, 0);
IPAddress dns(8, 8, 8, 8);


// healthy global variables
WebSocketsClient websocket;
int last_update;
bool connected_to_server;


void webSocketEvent(
  WStype_t type, 
  uint8_t * payload, 
  size_t length
) {
  switch(type) {
    case WStype_DISCONNECTED:
      Serial.printf("websocket disconnected\n");
      connected_to_server = false;
      break;
        
    case WStype_CONNECTED:
      Serial.printf("websocket connected: %s\n", payload);
      connected_to_server = true;
      websocket.sendTXT("connected");
      break;
        
    case WStype_TEXT:
      Serial.printf("websocket text response: %s\n", payload);
      break;
        
    case WStype_BIN:
      Serial.printf("websocket binary response: %u\n", length);
      hexdump(payload, length);
      break;
        
    case WStype_PING:
      // pong will be send automatically
      Serial.printf("ping\n");
      break;
            
    case WStype_PONG:
      // answer to a ping we send
      Serial.printf("pong\n");
      break;
  }
}


void setup() {
  Serial.begin(9600);

  if (!dhcp) {
    WiFi.config(
      static_ip, 
      subnet, 
      gateway, 
      dns
    );
  }

  Serial.print("connecting to " + String(ssid));
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    Serial.println("thinking about life while status is " + WiFi.status());
    delay(1000);
  }

  Serial.println("\nthe answer is 42 ");
  Serial.print("local ip at " + String(ssid) + " is ");
  Serial.println(WiFi.localIP());
  Serial.println("connecting to ws://" + String(server) + ":" + String(port));
  websocket.begin(server, port, "/");
  websocket.onEvent(webSocketEvent);
}

int get_temp() {
  return 80;
}

void loop() {
  if (WiFi.status() == WL_CONNECTED){
    websocket.loop();

    if (connected_to_server && last_update + update_freq < millis()){
      int temp = get_temp();
      String test = ("{\"temp\": \"" + String(temp) + "\"}");
      Serial.println("{\"temp\": \"" + String(temp) + "\"}");
      websocket.sendTXT(test);
      last_update = millis();
    }
  }
}
