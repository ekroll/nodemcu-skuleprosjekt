const connect = require("@databases/sqlite")
const {sql} = require("@databases/sqlite")
const ws = require('ws')


function get_db(path) {
    try {
        let db = connect(path)
        db.query(sql`
            CREATE TABLE IF NOT EXISTS temperature (
                time VARCHAR NOT NULL PRIMARY KEY,
                temp REAL NOT NULL
            );
        `)

        return db
    } 
    
    catch (error) {
        console.log(error)
        return
    }
}


function log_temp(db, temp) {
    let currentDate = new Date()
    let time = currentDate.getHours() + ":" + currentDate.getMinutes() + ":" + currentDate.getSeconds()
    
    try {
        db.query(sql`INSERT INTO temperature (time, temp) VALUES (${time}, ${temp})`)
        console.log(`temperature: ${temp}`)
    }

    catch (error) {
        console.log(error)
        console.log("failed to log data")
    }
}


function main() {
    const db = get_db("./src/server/data/database.db")
    const server = new ws.WebSocketServer({
        port: 8081
    })

    server.on('connection', function(socket) {
        socket.on('close', function() {
            console.log("client disconnected")
        })

        socket.on('message', function(msg) {
            let data
            
            try {
                data = JSON.parse(msg)
            } 
            
            catch (error) {
                console.log(msg)
            }

            try {
                temp = data["temp"]
                log_temp(db, temp)
            } 
            
            catch (error) {
                console.log(error)
            }
        })
    })
}


if (require.main === module) {
    main()
}